# == Description ==
Phone Guard is an electronic lock mechanism that will trigger an alarm when the USB cable is removed from the device. This can be used in different ways:

- Use a device as exhibit without pricy anti-theft protection
- Charge phone in a public place without watching after it
- Annoy your friends :P

This application has been developed by Katharina Sabel, part of the Random Robots Softworks studio. Check out more of our work at www.2rsoftworks.de or visit me personally at www.katharinasabel.de
The application will soon be released on Google Play for free as well as in a support package enabling you to support our development.