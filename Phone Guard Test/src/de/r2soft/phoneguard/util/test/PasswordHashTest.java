/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */

package de.r2soft.phoneguard.util.test;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import junit.framework.Assert;

import org.junit.Test;

import de.r2soft.phoneguard.util.PasswordHash;
import android.test.AndroidTestCase;

public class PasswordHashTest extends AndroidTestCase {

  /**
   * Test method for method validatePassword
   * 
   * @throws NoSuchAlgorithmException
   * @throws InvalidKeySpecException
   */
  @Test
  public void testValidatePassword() throws NoSuchAlgorithmException, InvalidKeySpecException {
	String password_bob = "bob";
	String hash_bob = PasswordHash.createHash(password_bob);
	String hash_bob_2 = PasswordHash.createHash(password_bob);

	boolean equal1 = PasswordHash.validatePassword(password_bob, hash_bob);
	Assert.assertEquals(true, equal1);

	boolean notEq1 = hash_bob.equals(hash_bob_2);
	Assert.assertEquals(false, notEq1);

  }

}
