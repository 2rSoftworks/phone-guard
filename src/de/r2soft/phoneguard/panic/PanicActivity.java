/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */
package de.r2soft.phoneguard.panic;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import android.app.Activity;
import android.app.Application;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import de.r2soft.phoneguard.R;
import de.r2soft.phoneguard.core.GuardianService;
import de.r2soft.phoneguard.core.MainActivity;
import de.r2soft.phoneguard.settings.AppSettings;
import de.r2soft.phoneguard.util.PasswordHash;
import de.r2soft.phoneguard.util.StatusWidget;
import de.r2soft.phoneguard.util.Utility;

public class PanicActivity extends Activity {

  private TextView hello;
  private EditText key;
  private Button login;

  @Override
  protected void onCreate(Bundle states) {
	super.onCreate(null);
	setContentView(R.layout.screen_unlock);

	hello = (TextView) this.findViewById(R.id.user_greeting);
	key = (EditText) this.findViewById(R.id.password_field);
	login = (Button) this.findViewById(R.id.login_button);

	hello.setText(R.string.screen_login_alarm);

	key.requestFocus();
	InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	imm.showSoftInput(key, InputMethodManager.SHOW_IMPLICIT);

	login.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {
		String password = key.getText().toString();

		if (password != null)
		  requestLogin(password);
	  }
	});
  }

  protected void requestLogin(String password) {

	String code = AppSettings.getGlobalPin(this);
	String temp = null;
	try {
	  temp = PasswordHash.createHash(password);
	}
	catch (NoSuchAlgorithmException e) {
	  e.printStackTrace();
	  new Exception(
		  "A bunch of monkeys were dispatched to find an algorythm but they all came back with bananas");
	}
	catch (InvalidKeySpecException e) {
	  e.printStackTrace();
	}

	InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	imm.hideSoftInputFromWindow(key.getWindowToken(), 0);

	/** Start login */
	if (temp.equals(code)) {

	  AppSettings.setCoreEnabled(false, this);
	  startActivity(new Intent(this, MainActivity.class));
	  stopService(new Intent(this, GuardianService.class));
	  updateWidget();
	}
	else {
	  Toast.makeText(this, R.string.toast_login_failed, Toast.LENGTH_SHORT).show();
	}
  }

  @Override
  public void onBackPressed() {
  }

  /** Overriding navigational methods so that the "thief" can't escape to the home screen */
  @Override
  public void onStop() {
	if (AppSettings.isCoreEnabled(this)) {

	}
	super.onStop();
  }

  @Override
  public void onPause() {
	if (AppSettings.isCoreEnabled(this)) {

	}
	super.onPause();

  }

  /**
   * Inform the widget to pull the new application state.
   */
  private void updateWidget() {
	Application app = getApplication();
	Intent intent = new Intent(app, StatusWidget.class);
	intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
	int ids[] = AppWidgetManager.getInstance(app).getAppWidgetIds(
		new ComponentName(app, StatusWidget.class));
	intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
	sendBroadcast(intent);
  }

}
