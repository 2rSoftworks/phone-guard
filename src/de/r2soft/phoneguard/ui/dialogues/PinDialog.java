/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */
package de.r2soft.phoneguard.ui.dialogues;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import de.r2soft.phoneguard.R;
import de.r2soft.phoneguard.settings.AppSettings;
import de.r2soft.phoneguard.util.Utility;

/**
 * This fragment will be displayed if the user opens the app for the first time
 * 
 * @author Katharina
 * 
 */
@Deprecated
public class PinDialog extends DialogFragment {

  private EditText text;
  private Button next;
  private Dialog d;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle states) {
	View v = inflater.inflate(R.layout.dialog_pin, container, false);
	d = this.getDialog();

	d.setTitle(R.string.dialog_setup_title);
	d.setCanceledOnTouchOutside(false);

	next = (Button) v.findViewById(R.id.set_pin_button);
	text = (EditText) v.findViewById(R.id.dialog_setup_field_pin);

	next.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {

		if (text.getText().toString() != null && text.getText().toString().length() >= 4) {
		  Toast.makeText(getActivity(), R.string.toast_setup_pin_saved, Toast.LENGTH_SHORT).show();
		  String pin = Utility.makeHashKey(text.getText().toString());
		  AppSettings.setGlobalPin(pin, getActivity());
		  AppSettings.setGlobalFirst(false, getActivity());
		  dismiss();
		}
		else {
		  Toast.makeText(getActivity(), R.string.toast_setup_pin_failed, Toast.LENGTH_LONG).show();
		}

	  }
	});

	return v;
  }
}
