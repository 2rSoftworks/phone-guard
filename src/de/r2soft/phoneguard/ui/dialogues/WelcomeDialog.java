/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */
package de.r2soft.phoneguard.ui.dialogues;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import de.r2soft.phoneguard.R;
import de.r2soft.phoneguard.core.LoginActivity;
import de.r2soft.phoneguard.settings.AppSettings;

/**
 * This fragment will be displayed if the user opens the app for the first time
 * 
 * @author Katharina
 * 
 */
@Deprecated
public class WelcomeDialog extends DialogFragment {

  private Button next;
  private Dialog d;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle states) {
	View v = inflater.inflate(R.layout.dialog_welcome, container, false);
	d = this.getDialog();

	/** Set applications Donation state back to false */

	d.setTitle(R.string.dialog_setup_title);
	d.setCanceledOnTouchOutside(false);
	d.setCancelable(false);

	next = (Button) v.findViewById(R.id.dialog_setup_button);
	next.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {
		FragmentManager woman = getFragmentManager();
		PinDialog fragment = new PinDialog();
		fragment.show(woman, "fragment_setup_wizard");
		LoginActivity.settingup = false;
		dismiss();
	  }
	});

	return v;
  }

}
