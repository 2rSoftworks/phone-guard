/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */
package de.r2soft.phoneguard.ui.dialogues;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.android.vending.billing.IInAppBillingService;

import de.r2soft.phoneguard.R;
import de.r2soft.phoneguard.settings.AppSettings;

public class DonateDialog extends DialogFragment {

  private final Logger log = Logger.getLogger(getClass().getSimpleName());
  private final String[] DONATION_OPTIONS = { "studio_donation_small" };

  private ImageView playstore;
  private Button no;
  private Dialog d;

  private IInAppBillingService billService;
  ArrayList<DonationOption> donationOptions;

  @Override
  public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	// Bind in-App billing service
	donationOptions = new ArrayList<DonationOption>();
	String BILLING = "com.android.vending.billing.InAppBillingService.BIND";
	Intent billIntent = new Intent(BILLING);
	getActivity().bindService(billIntent, serviceConnection, Activity.BIND_AUTO_CREATE);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle states) {
	View v = inflater.inflate(R.layout.dialog_donate, container, false);
	d = this.getDialog();

	d.setTitle(R.string.dialog_donation_title);

	/** Make sure this will not be displayed again */
	AppSettings.setDonateAsk(false, getActivity());
	AppSettings.setCounterState(false, getActivity());

	playstore = (ImageView) v.findViewById(R.id.donate_playstore);
	playstore.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {

		/* This will make the user give us money :) Hopefully */
		// ArrayList<String> list = new ArrayList<String>();
		// list.add("studioDonation");
		// Bundle bundle = new Bundle();
		// bundle.putStringArrayList("ITEM_ID_LIST", list);

		if (donationOptions.size() > 0) {
		  purchaseItem(donationOptions.get(0));
		}

	  }
	});

	no = (Button) v.findViewById(R.id.donate_notnow);
	no.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {
		dismiss();
	  }
	});

	return v;
  }

  @Override
  public void dismiss() {
	getActivity().unbindService(serviceConnection);
	super.dismiss();
  }

  /** In-App billing connection service */
  ServiceConnection serviceConnection = new ServiceConnection() {
	@Override
	public void onServiceDisconnected(ComponentName name) {
	  log.info("Billing service disconnected");
	  billService = null;
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
	  log.info("Billing service connected");
	  billService = IInAppBillingService.Stub.asInterface(service);

	  retrieveGooglePlayData.run();
	}
  };

  /**
   * This Runnable retrieves information from the google play store. It is moved out of the main
   * thread because the network operation would be blocking the main thread.
   */
  private Runnable retrieveGooglePlayData = new Runnable() {
	public void run() {
	  // Create required bundles
	  Bundle querySkus = new Bundle();
	  Bundle skuDetails = new Bundle();

	  // Load the available donation options.
	  ArrayList<String> skuList = new ArrayList<String>(Arrays.asList(DONATION_OPTIONS));
	  querySkus.putStringArrayList("ITEM_ID_LIST", skuList);

	  // Attempt to retrieve information from Google Play
	  try {
		skuDetails = billService.getSkuDetails(3, getActivity().getPackageName(), "inapp",
			querySkus);
	  }
	  catch (RemoteException e) {
		log.severe("Could not retieve in app purchase options from google play");
		log.severe(e.getMessage());
	  }

	  int response = skuDetails.getInt("RESPONSE_CODE");
	  if (response == 0) {
		ArrayList<String> responseList = skuDetails.getStringArrayList("DETAILS_LIST");

		try {
		  for (String thisResponse : responseList) {
			JSONObject object = new JSONObject(thisResponse);
			String sku = object.getString("productId");
			String price = object.getString("price");
			String title = object.getString("title");
			String description = object.getString("description");
			donationOptions.add(new DonationOption(sku, price, title, description));
		  }
		}
		catch (JSONException e) {
		  log.severe("Unable to process Google Play JSON response");
		  log.severe(e.getMessage());
		}
	  }
	}
  };

  /**
   * Start the purchase process for a given DonationOption.
   * 
   * @param item
   */
  private void purchaseItem(DonationOption item) {
	if (billService != null) {
	  try {
		Bundle buyIntentBundle = billService.getBuyIntent(3, getActivity().getPackageName(),
			item.sku, "inapp", "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
		PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");

		getActivity().startIntentSenderForResult(pendingIntent.getIntentSender(), 1001,
			new Intent(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0));
	  }
	  catch (RemoteException e) {
		log.severe("Communication error with the Billing Service.");
		log.severe(e.getMessage());
	  }
	  catch (SendIntentException e) {
		log.severe("Could not send purchase intent to Google Play store.");
		log.severe(e.getMessage());
	  }
	}
	else {
	  log.severe("Billing service is not available");
	}
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
	log.info("Receiving result");
  }

  /**
   * This inner class holds information on donation options
   */
  private class DonationOption {
	public final String sku;
	public final String price;
	public final String title;
	public final String description;

	public DonationOption(String sku, String price, String title, String description) {
	  this.sku = sku;
	  this.price = price;
	  this.title = title;
	  this.description = description;
	}

	@Override
	public String toString() {
	  return "DonationOption [sku=" + sku + ", price=" + price + ", title=" + title
		  + ", description=" + description + "]";
	}
  }

}