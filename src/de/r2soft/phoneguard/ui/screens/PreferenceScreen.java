/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */
package de.r2soft.phoneguard.ui.screens;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.telephony.PhoneNumberUtils;
import android.text.InputType;
import android.widget.Toast;
import de.r2soft.phoneguard.R;
import de.r2soft.phoneguard.core.LoginActivity;
import de.r2soft.phoneguard.settings.AppSettings;
import de.r2soft.phoneguard.ui.dialogues.DonateDialog;

public class PreferenceScreen extends PreferenceFragment implements
	OnSharedPreferenceChangeListener, OnPreferenceClickListener {

  private Context context;
  private SwitchPreference core, location;
  // private CheckBoxPreference silent, directional, maps;
  private EditTextPreference phone, email;
  private Preference reset, credits, donate, bugs;
  private PreferenceCategory general;

  private boolean gps;

  @Override
  public void onCreate(Bundle savedInstanceState) {

	super.onCreate(savedInstanceState);
	addPreferencesFromResource(R.xml.preferences);
	this.context = getActivity();
	getPreferenceManager().setSharedPreferencesName(getString(R.string.key_global_name));
	getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

	/* Categories */
	general = (PreferenceCategory) findPreference(getString(R.string.key_preference_group_general));

	/* Switches */
	core = (SwitchPreference) findPreference(getString(R.string.key_preference_switch_core));
	location = (SwitchPreference) findPreference(getString(R.string.key_preference_switch_location));

	/* Checkboxes */
	// silent = (CheckBoxPreference)
	// findPreference(getString(R.string.key_preference_checkbox_silent));
	// directional = (CheckBoxPreference)
	// findPreference(getString(R.string.key_preference_checkbox_location_directional));
	// TODO: maps = (CheckBoxPreference) findPreference(getString(R.string.key_location_maps));

	/* EditTexts */
	phone = (EditTextPreference) findPreference(getString(R.string.key_preference_edittext_location_phonenumber));
	// email = (EditTextPreference)
	// findPreference(getString(R.string.key_preference_edittext_location_email));

	/* Generic Preferences */
	// private Preference reset, credits, donate, bugs;
	reset = (Preference) findPreference(getString(R.string.key_preference_preference_pinreset));
	credits = (Preference) findPreference(getString(R.string.key_preference_preference_credits));
	donate = (Preference) findPreference(getString(R.string.key_preference_preference_donate));
	bugs = (Preference) findPreference(getString(R.string.key_preference_preference_reportbug));

	/* Setups */
	phone.getEditText().setRawInputType(
		InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
	phone.getEditText().setRawInputType(
		InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);

	this.setDirectListener();
	this.checkGPS();
	this.updateUI();
  }

  private void checkGPS() {
	if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
	  gps = true;
	}
	else {
	  gps = false;
	}
  }

  private void setDirectListener() {

	reset.setOnPreferenceClickListener(new OnPreferenceClickListener() {

	  @Override
	  public boolean onPreferenceClick(Preference preference) {

		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		  @Override
		  public void onClick(DialogInterface dialog, int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
			  AppSettings.setGlobalFirst(true, context);
			  AppSettings.setGlobalPin(null, context);
			  startActivity(new Intent(context, LoginActivity.class));
			  getFragmentManager().popBackStack();
			  break;

			case DialogInterface.BUTTON_NEGATIVE:
			  break;
			}
		  }
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder
			.setMessage(context.getString(R.string.screen_preference_pref_reset_title))
			.setPositiveButton(context.getString(R.string.screen_preference_pref_reset_dialog_yes),
				dialogClickListener)
			.setNegativeButton(context.getString(R.string.screen_preference_pref_reset_dialog_no),
				dialogClickListener).show();

		return true;
	  }
	});

	credits.setOnPreferenceClickListener(new OnPreferenceClickListener() {

	  @Override
	  public boolean onPreferenceClick(Preference preference) {
		FragmentManager woman = getFragmentManager();
		FragmentTransaction trans = woman.beginTransaction();
		trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		trans.addToBackStack(null);
		trans.replace(R.id._master_container, new CreditScreen());
		trans.commit();

		return true;
	  }
	});

	donate.setOnPreferenceClickListener(new OnPreferenceClickListener() {

	  @Override
	  public boolean onPreferenceClick(Preference preference) {
		FragmentManager woman = getFragmentManager();
		DonateDialog donate = new DonateDialog();
		donate.show(woman, "fragment_donation_hint");

		return true;
	  }
	});

	bugs.setOnPreferenceClickListener(new OnPreferenceClickListener() {

	  @Override
	  public boolean onPreferenceClick(Preference preference) {
		Intent email = new Intent(Intent.ACTION_SEND);
		email.putExtra(Intent.EXTRA_EMAIL,
			new String[] { getString(R.string.screen_preference_pref_bugs_email_address) });
		email.putExtra(Intent.EXTRA_SUBJECT,
			getString(R.string.screen_preference_pref_bugs_email_topic));
		email.putExtra(Intent.EXTRA_TEXT,
			getString(R.string.screen_preference_pref_bugs_email_body)
				+ getString(R.string.global_name) + ": ");
		email.setType("message/rfc822");
		startActivity(Intent.createChooser(email,
			getString(R.string.screen_preference_pref_bugs_email_picker)));
		return true;
	  }
	});
  }

  @Override
  public void onResume() {
	super.onResume();
	getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	updateUI();

  }

  @Override
  public void onPause() {
	super.onPause();
	getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

	/* Core status */
	if (key.equals(getString(R.string.key_preference_switch_core))) {
	  AppSettings.setCoreEnabled(
		  sharedPreferences.getBoolean(getString(R.string.key_preference_switch_core), false),
		  context);
	}
	// /* Silent core */
	// else if (key.equals(getString(R.string.key_preference_checkbox_silent))) {
	// AppSettings.setCoreSilent(
	// sharedPreferences.getBoolean(getString(R.string.key_preference_checkbox_silent), false),
	// context);
	// }
	/* Location status */
	else if (key.equals(getString(R.string.key_preference_switch_location))) {
	  AppSettings.setLocationEnabled(
		  sharedPreferences.getBoolean(getString(R.string.key_preference_switch_location), false),
		  context);
	}
	/* Directional guiding */
	else if (key.equals(getString(R.string.key_preference_checkbox_location_directional))) {
	  AppSettings.setLocationDirectional(sharedPreferences.getBoolean(
		  getString(R.string.key_preference_checkbox_location_directional), false), context);
	}
	/* Setting Phonenumber */
	else if (key.equals(getString(R.string.key_preference_edittext_location_phonenumber))) {
	  String number = sharedPreferences.getString(
		  getString(R.string.key_preference_edittext_location_phonenumber), null);
	  if (number != null) {
		if (PhoneNumberUtils.isGlobalPhoneNumber(number)
			&& PhoneNumberUtils.isWellFormedSmsAddress(number)) {
		  Toast.makeText(context, getString(R.string.toast_preferences_number_vaild),
			  Toast.LENGTH_SHORT).show();
		  AppSettings.setLocationPhonenumber(number, context);
		}
		else {
		  Toast.makeText(context, getString(R.string.toast_preferences_number_invaild),
			  Toast.LENGTH_SHORT).show();
		}
	  }
	  else {
		Toast.makeText(context, getString(R.string.toast_preferences_number_null),
			Toast.LENGTH_SHORT).show();
	  }
	}
	this.updateUI();

  }

  private void updateUI() {
	if (AppSettings.isCoreEnabled(context)) {
	  OverviewScreen.makeService();
	  location.setEnabled(true);
	  // silent.setEnabled(true);

	  if (AppSettings.isLocationEnabled(context)) {
		// directional.setEnabled(true);
		// TODO: maps.setEnabled(true);
		phone.setEnabled(true);
		// TODO: email.setEnabled(true);
	  }
	}
	else {
	  OverviewScreen.killService();
	  location.setEnabled(false);
	  // silent.setEnabled(false);
	  // directional.setEnabled(false);
	  // TODO: maps.setEnabled(false);
	  phone.setEnabled(false);
	}
	if (!AppSettings.isLocationEnabled(context)) {
	  // directional.setEnabled(false);
	  // TODO: maps.setEnabled(false);
	  phone.setEnabled(false);
	  // TODO: email.setEnabled(false);
	}

	/* Check the Preferences according to the App States */
	core.setChecked(AppSettings.isCoreEnabled(context));
	location.setChecked(AppSettings.isLocationEnabled(context));
	// silent.setChecked(AppSettings.isCoreSilent(context));
	// directional.setChecked(AppSettings.isLocationDirectional(context));
  }

  @Override
  public boolean onPreferenceClick(Preference preference) {
	// TODO: Change the preference clicks to this global listener
	return false;
  }

}
