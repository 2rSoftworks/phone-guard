/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */
package de.r2soft.phoneguard.ui.screens;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import de.r2soft.phoneguard.R;
import de.r2soft.phoneguard.core.GuardianService;
import de.r2soft.phoneguard.settings.AppSettings;
import de.r2soft.phoneguard.ui.dialogues.DonateDialog;

public class OverviewScreen extends Fragment {

  private static Context context;
  private View v;
  private ImageView imagine;
  private ToggleButton switcher;
  private TextView text;
  private boolean current;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle states) {
	v = inflater.inflate(R.layout.screen_overview, container, false);
	context = getActivity();

	imagine = (ImageView) v.findViewById(R.id.screen_overview_status_view);
	text = (TextView) v.findViewById(R.id.screen_overview_status_text);
	switcher = (ToggleButton) v.findViewById(R.id.screen_overview_toggle);
	makeUI();
	makeService();

	/** If it's time to ask the user for money */
	if (AppSettings.isDonateAsk(getActivity())) {
	  donations();
	}

	switcher.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {

		current = AppSettings.isCoreEnabled(getActivity());
		AppSettings.setCoreEnabled(!current, getActivity());
		makeUI();
		makeService();
	  }
	});

	return v;
  }

  private void donations() {
	FragmentManager woman = getFragmentManager();
	DonateDialog donate = new DonateDialog();
	donate.show(woman, "fragment_donation_hint");
  }

  public static void makeService() {
	if (AppSettings.isCoreEnabled(context)) {
	  Intent i = new Intent(context, GuardianService.class);
	  context.startService(i);
	}
  }

  public static void killService() {
	if (!AppSettings.isCoreEnabled(context)) {
	  Intent i = new Intent(context, GuardianService.class);
	  context.stopService(i);
	}
  }

  private void makeUI() {
	if (AppSettings.isCoreEnabled(getActivity())) {
	  imagine.setImageResource(R.drawable.icon_guarding_phone_full);
	  switcher.setChecked(true);
	  text.setText(R.string.screen_overview_text_on);
	  if (!isConnected(getActivity())) {
		Toast.makeText(getActivity(), R.string.toast_overview_not_plugged, Toast.LENGTH_LONG)
			.show();
	  }
	}
	else {
	  context.stopService(new Intent(context, GuardianService.class));
	  switcher.setChecked(false);
	  text.setText(R.string.screen_overview_text_off);
	  imagine.setImageResource(R.drawable.icon_disabled_full);
	}
  }

  public static boolean isConnected(Context context) {
	Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
	int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
	return plugged == BatteryManager.BATTERY_PLUGGED_AC
		|| plugged == BatteryManager.BATTERY_PLUGGED_USB;
  }
}
