/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */
package de.r2soft.phoneguard.core;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import de.r2soft.phoneguard.R;
import de.r2soft.phoneguard.settings.AppSettings;
import de.r2soft.phoneguard.setup.SetupContainer;
import de.r2soft.phoneguard.util.PasswordHash;

public class LoginActivity extends Activity {

  private EditText key;
  private Button login;

  @Override
  protected void onCreate(Bundle states) {
	super.onCreate(null);

	setContentView(R.layout.screen_unlock);
	getFragmentManager().popBackStack();

	if (AppSettings.isGlobalFirst(this)) {
	  introductory();
	}

	key = (EditText) this.findViewById(R.id.password_field);
	login = (Button) this.findViewById(R.id.login_button);

	key.requestFocus();
	InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	imm.showSoftInput(key, InputMethodManager.SHOW_IMPLICIT);

	login.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {
		String password = key.getText().toString();
		System.out.println(password);

		if (password != null)
		  try {
			requestLogin(password);
		  }
		  catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		  }
		  catch (InvalidKeySpecException e) {
			e.printStackTrace();
		  }
		  catch (NotFoundException e) {
			e.printStackTrace();
		  }

	  }
	});

  }

  public static boolean settingup;

  private void introductory() {

	Intent setupIntent = new Intent(this, SetupContainer.class);
	startActivityForResult(setupIntent, 0);
  }

  private void forgotYourPasswordHaveYouNow() {
	if (AppSettings.isCoreEnabled(this)) {
	  final AlertDialog noDialog = new AlertDialog.Builder(this).create();

	  // Setting Dialog Title
	  noDialog.setTitle(this.getString(R.string.dialog_reset_title_no));

	  // Setting Dialog Message
	  noDialog.setMessage(this.getString(R.string.dialog_reset_body_no));

	  // Setting Icon to Dialog
	  noDialog.setIcon(R.drawable.gloss_icon_deny);

	  // Setting OK Button
	  noDialog.setButton(AlertDialog.BUTTON_NEUTRAL,
		  this.getString(R.string.dialog_reset_button_no), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			  noDialog.dismiss();
			}
		  });

	  // Showing Alert Message
	  noDialog.show();
	}
	else {
	  final AlertDialog yesDialog = new AlertDialog.Builder(this).create();

	  // Setting Dialog Title
	  yesDialog.setTitle(this.getString(R.string.dialog_reset_title_yes));

	  // Setting Dialog Message
	  yesDialog.setMessage(this.getString(R.string.dialog_reset_body_yes));

	  // Setting Icon to Dialog
	  yesDialog.setIcon(R.drawable.gloss_icon_accept);

	  // Setting OK Button
	  yesDialog.setButton(AlertDialog.BUTTON_NEUTRAL,
		  this.getString(R.string.dialog_reset_button_yes), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			  yesDialog.dismiss();
			  introductory();
			  // FragmentManager woman = getFragmentManager();
			  // PinDialog pinSetting = new PinDialog();
			  // pinSetting.show(woman, "fragment_pin_resetting");
			}
		  });

	  // Showing Alert Message
	  yesDialog.show();
	}
  }

  @Override
  public void onResume() {
	super.onResume();
	getFragmentManager().popBackStack();

	if (AppSettings.isGlobalFirst(this))
	  introductory();

	if (AppSettings.isCounterState(this))
	  AppSettings.incrementUsageCounter(this);

	/** Check if Donation Fragment should be shown */
	if (AppSettings.getUsageCounter(this) > 4 && AppSettings.isCounterState(this)) {
	  AppSettings.setDonateAsk(true, this);
	}

  }

  protected void requestLogin(String password) throws NoSuchAlgorithmException,
	  InvalidKeySpecException, NotFoundException {

	String actual = AppSettings.getGlobalPin(this);

	if (actual == null) {
	  Toast.makeText(this, getString(R.string.toast_login_null), Toast.LENGTH_LONG).show();
	}
	else {
	  InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	  imm.hideSoftInputFromWindow(key.getWindowToken(), 0);

	  key.setText("");

	  /** Start login */
	  if (PasswordHash.validatePassword(password, actual)) {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	  }
	  else {
		Toast.makeText(this, R.string.toast_login_failed, Toast.LENGTH_SHORT).show();
	  }
	}

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
	getMenuInflater().inflate(R.menu.menu_login, menu);
	return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

	/** Just good practise checking what item was selected */
	switch (item.getItemId()) {
	case R.id.menu_help:
	  forgotYourPasswordHaveYouNow();
	  break;

	default:
	  break;
	}
	return false;
  }

  /** Will close the application of Back button is pressed */
  @Override
  public void onBackPressed() {
	getFragmentManager().popBackStack();
	super.onStop();
	super.finish();
  }
}
