/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */
package de.r2soft.phoneguard.core;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;
import de.r2soft.phoneguard.R;
import de.r2soft.phoneguard.settings.AppSettings;

/** Displaying a notification if the service and thus phone guard is running */
public class GuardianService extends Service {

  private enum DIRECTION {
	N, S, W, E,

	NE, NW, SE, SW,

	FAILED;
  }

  private static String TAG = GuardianService.class.getSimpleName();

  /** Receiver stuff */
  private boolean alarm;
  private int oldVolume;

  /** Alarm */
  private SoundPool sounds;
  private int alarmId;
  AudioManager audioManager;

  @Override
  public void onCreate() {
	super.onCreate();
	alarm = false;

	sounds = new SoundPool(1, AudioManager.STREAM_ALARM, 0);
	alarmId = sounds.load(this, R.raw.alarm_mp3, 1);

	/** Create receiver */
	registerReceiver(receiver, new IntentFilter(Intent.ACTION_POWER_DISCONNECTED));
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {

	String title = getString(R.string.notification_title);
	String msg = getString(R.string.notification_msg);
	Integer icon = R.drawable.notification_guarding_phone;
	Intent i = new Intent(this, LoginActivity.class);
	PendingIntent pendingIntent = PendingIntent.getActivity(getApplication(), 0, i,
		Intent.FLAG_ACTIVITY_CLEAR_TASK);

	NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext())
		.setContentTitle(title).setContentText(msg).setSmallIcon(icon)
		.setContentIntent(pendingIntent);

	startForeground(Notification.FLAG_FOREGROUND_SERVICE, builder.build());

	return START_STICKY;

  }

  @Override
  public void onDestroy() {
	super.onDestroy();

	stopForeground(true);
	unregisterReceiver(receiver);

	if (alarm) {
	  sounds.stop(alarmId);
	  audioManager.setStreamVolume(AudioManager.STREAM_ALARM, oldVolume, 0);
	}
	if (AppSettings.isLocationEnabled(getApplication()))
	  sendGPSLocation.cancel();
  }

  @Override
  public IBinder onBind(Intent intent) {
	Log.e(TAG, "onBind is not implemented in " + getClass().getName());
	return null;
  }

  BroadcastReceiver receiver = new BroadcastReceiver() {

	@Override
	public void onReceive(Context context, Intent intent) {
	  alarm = true;

	  audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

	  /** Save old volume to be restored after the alarm */
	  oldVolume = audioManager.getStreamVolume(AudioManager.STREAM_ALARM);

	  /** Play alarm in a loop at the maximum noise */
	  audioManager.setStreamVolume(AudioManager.STREAM_ALARM,
		  audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM), 0);
	  sounds.play(alarmId, 1f, 1f, 1, -1, 1f);

	  if (AppSettings.isLocationEnabled(context)) {
		startLocationTracking();
		new Timer().scheduleAtFixedRate(sendGPSLocation, 1000, 60000);
	  }

	  /** Start panic activity so that the user can disable the alarm */
	  Intent i = new Intent(getApplicationContext(), AlarmActivity.class);
	  i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	  startActivity(i);
	}
  };

  TimerTask sendGPSLocation = new TimerTask() {

	@Override
	public void run() {

	  String phoneNumber = AppSettings.getLocationPhonenumber(getApplicationContext());
	  SmsManager smsManager = SmsManager.getDefault();
	  String message = "";

	  /** If there is a phone number */
	  if (phoneNumber != null) {

		/** Send directional data */
		if (AppSettings.isLocationDirectional(getApplicationContext())) {

		  /** If there is no last location being stored do this */
		  if (AppSettings.getLocationDirectionalLast(getApplicationContext()) == null) {
			storeNewLocation();

		  }
		  /** If there already was a location saved in the settings! Direct from there */
		  else {

			double oldLat = AppSettings.getLocationDirectionalLast(getApplicationContext())
				.getLatitude();
			double oldLong = AppSettings.getLocationDirectionalLast(getApplicationContext())
				.getLatitude();

			Vector2D oldLocation = new Vector2D(oldLat, oldLong);
			Vector2D newLocation = new Vector2D(0, 0);

			/** Check if the locations aren't null! */
			if (netLocation != null && gpsLocation != null) {

			  /** use GPS Location */
			  if (gpsLocation.getAccuracy() <= netLocation.getAccuracy()) {
				newLocation.add(new Vector2D(gpsLocation.getLatitude(), 0));
				newLocation.add(new Vector2D(0, gpsLocation.getLongitude()));
			  }
			  /** Use Network Location */
			  else {
				newLocation.add(new Vector2D(netLocation.getLatitude(), 0));
				newLocation.add(new Vector2D(0, netLocation.getLongitude()));
			  }
			}

			/** This is where the magic happens */

			Vector2D directionVector = newLocation.subtract(oldLocation);

			DIRECTION relativeDirection = getLocationalDirection(directionVector);

			if (relativeDirection != DIRECTION.FAILED) {
			  message = getString(R.string.message_relative_direction)
				  + relativeDirection.toString();
			  smsManager.sendTextMessage(phoneNumber, null, message, null, null);
			}
			else {
			  Log.i(TAG, "FATAL ERROR: COULD NOT RETRIEVE RELATIVE DIRECTION!");
			}

		  }
		}

		/** Send normal GPS coordinates */
		else {

		  if (netLocation != null && gpsLocation != null) {
			if (gpsLocation.getAccuracy() <= netLocation.getAccuracy()) {
			  message = useGPSLocation();
			}
			else {
			  message = useNetworkLocation();
			}
		  }
		  else if (netLocation != null) {
			message = useNetworkLocation();
		  }
		  else if (gpsLocation != null) {
			message = useGPSLocation();
		  }
		  else {
			// No location is available
			message = getString(R.string.feedback_location_null);
		  }

		  smsManager.sendTextMessage(phoneNumber, null, message, null, null);
		}

	  }
	  else {
		Toast.makeText(getApplicationContext(), getString(R.string.toast_preferences_number_null),
			Toast.LENGTH_SHORT).show();
	  }
	}

	private DIRECTION getLocationalDirection(Vector2D direction) {
	  double x = direction.getX();
	  double y = direction.getY();

	  if (x > 0 && y == 0) {
		return DIRECTION.E;
	  }
	  else if (x < 0 && y == 0) {
		return DIRECTION.W;
	  }
	  else if (x == 0 && y < 0) {
		return DIRECTION.S;
	  }
	  else if (x == 0 && y > 0) {
		return DIRECTION.N;
	  }
	  else if (x > 0 && y > 0) {
		return DIRECTION.NE;
	  }
	  else if (x < 0 && y > 0) {
		return DIRECTION.NW;
	  }
	  else if (x > 0 && y < 0) {
		return DIRECTION.SE;
	  }
	  else if (x < 0 && y < 0) {
		return DIRECTION.SW;
	  }
	  else {
		return DIRECTION.FAILED;
	  }

	}
  };

  private String useGPSLocation() {
	StringBuilder s = new StringBuilder();
	s.append(gpsLocation.getProvider() + ": ");
	s.append("Lat: ");
	s.append(gpsLocation.getLatitude());
	s.append("Long: ");
	s.append(gpsLocation.getLongitude());
	return s.toString();
  }

  private String useNetworkLocation() {
	StringBuilder s = new StringBuilder();
	s.append(netLocation.getProvider() + ": ");
	s.append("Lat: ");
	s.append(netLocation.getLatitude());
	s.append("Long: ");
	s.append(netLocation.getLongitude());
	return s.toString();
  }

  // Acquire a reference to the system Location Manager
  Location netLocation = null;
  Location gpsLocation = null;
  LocationManager locationManager = null;

  /**
   * Start GPS location tracking and initialize with the last know position.
   * 
   * @return
   */
  public void startLocationTracking() {

	locationManager = (LocationManager) getApplication().getSystemService(LOCATION_SERVICE);
	boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

	Log.i(TAG, "GPS: " + isGPSEnabled);
	Log.i(TAG, "Net: " + isNetworkEnabled);

	if (isNetworkEnabled) {
	  if (locationManager != null) {
		netLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,
			locationListener);
	  }
	}
	if (isGPSEnabled) {
	  if (locationManager != null) {
		gpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		locationManager
			.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
	  }
	}
  }

  /** Will be called every time a new location is available for saving when in directional mode */
  private void storeNewLocation() {

	/** Check if the locations aren't null! */
	if (netLocation != null && gpsLocation != null) {

	  /** Then check accuracy. And pick accordingly. Save the location in the AppSettings */
	  if (gpsLocation.getAccuracy() <= netLocation.getAccuracy()) {
		AppSettings.setLocationDirectionalLast(gpsLocation, getApplicationContext());
	  }
	  else {
		AppSettings.setLocationDirectionalLast(netLocation, getApplicationContext());
	  }
	}
	else if (netLocation != null) {
	  AppSettings.setLocationDirectionalLast(netLocation, getApplicationContext());
	}
	else if (gpsLocation != null) {
	  AppSettings.setLocationDirectionalLast(gpsLocation, getApplicationContext());
	}
	else {
	  /* No location is available */
	  Log.i(TAG, getString(R.string.feedback_location_null));
	}

  }

  // Define a listener that responds to location updates
  LocationListener locationListener = new LocationListener() {
	public void onLocationChanged(Location location) {

	  if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
		gpsLocation = location;
	  }
	  else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {
		netLocation = location;
	  }
	  else {
		Log.w(TAG, "Recieved unexpected location update in LockService. Received type was "
			+ location.getProvider());
	  }

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	public void onProviderEnabled(String provider) {
	}

	public void onProviderDisabled(String provider) {
	}
  };

}
