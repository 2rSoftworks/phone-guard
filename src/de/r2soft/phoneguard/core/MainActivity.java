/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */
package de.r2soft.phoneguard.core;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;

import com.android.vending.billing.IInAppBillingService;

import de.r2soft.phoneguard.R;
import de.r2soft.phoneguard.ui.screens.CreditScreen;
import de.r2soft.phoneguard.ui.screens.OverviewScreen;
import de.r2soft.phoneguard.ui.screens.PreferenceScreen;

public class MainActivity extends Activity {

  private CreditScreen credits;
  private PreferenceScreen settings;
  private IInAppBillingService billService;

  @Override
  protected void onCreate(Bundle states) {
	super.onCreate(states);
	setContentView(R.layout._master);

	// Bind in-app billing service
	bindService(new Intent("com.android.vending.billing.InAppBillingService.BIND"),
		serviceConnection, BIND_AUTO_CREATE);

	credits = new CreditScreen();
	settings = new PreferenceScreen();

	travelToOverview(false);

  }

  /** In-App billing connection service */
  ServiceConnection serviceConnection = new ServiceConnection() {
	@Override
	public void onServiceDisconnected(ComponentName name) {
	  billService = null;
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
	  billService = IInAppBillingService.Stub.asInterface(service);
	}
  };

  /** Displays the overview Fragment */
  private void travelToOverview(boolean returned) {
	FragmentManager woman = getFragmentManager();
	FragmentTransaction trans = woman.beginTransaction();
	if (returned) {
	  trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
	}
	else {
	  trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
	}
	Fragment fragment = new OverviewScreen();
	trans.replace(R.id._master_container, fragment);
	trans.commit();

  }

  /** For the settings fragment */
  public CreditScreen showCredits() {
	return credits;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

	FragmentManager woman = getFragmentManager();
	FragmentTransaction trans = woman.beginTransaction();
	trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
	trans.addToBackStack(null);

	/** Just good practise checking what item was selected */
	switch (item.getItemId()) {
	case R.id.menu_item_credits:

	  trans.replace(R.id._master_container, credits);
	  trans.commit();
	  break;

	case R.id.menu_settings:

	  trans.replace(R.id._master_container, settings);
	  trans.commit();
	  break;

	default:
	  break;
	}
	return false;
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
	getMenuInflater().inflate(R.menu.menu_secure, menu);
	return true;
  }

  @Override
  public void onBackPressed() {
	if (credits.isVisible() || settings.isVisible()) {
	  travelToOverview(true);
	}
	else {
	  Intent i = new Intent(this, LoginActivity.class);
	  startActivity(i);
	}
  }

  @Override
  protected void onDestroy() {
	super.onDestroy();
	if (serviceConnection != null) {
	  unbindService(serviceConnection);
	}
  }

  /** Ensuring that the user will be thrown into the Login Activity on application restart */
  @Override
  protected void onRestart() {
	super.onRestart();
	startActivity(new Intent(this, LoginActivity.class));
  }
}
