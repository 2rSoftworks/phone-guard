/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */

package de.r2soft.phoneguard.setup;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import de.r2soft.phoneguard.R;

public class Welcome_2_PIN extends Fragment {
  private EditText text;
  private ImageView indicator;
  private ScrollView scroll;
  private View view;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	view = inflater.inflate(R.layout.dialog_welcome_subscreen_2, container, false);

	text = (EditText) view.findViewById(R.id.dialog_setup_field_pin);
	indicator = (ImageView) view.findViewById(R.id.dialog_setup_view_strength);
	scroll = (ScrollView) view.findViewById(R.id.dialog_setup_scrollview);

	if (text.getText().length() < 4)
	  ((Button) getActivity().findViewById(R.id.welcome_screen_right_button)).setEnabled(false);
	else
	  ((Button) getActivity().findViewById(R.id.welcome_screen_right_button)).setEnabled(true);

	text.addTextChangedListener(new TextWatcher() {

	  public void afterTextChanged(Editable s) {
		if (text.getText().toString().length() < 4) {
		  indicator.setImageResource(R.drawable.strength_null);
		  ((Button) getActivity().findViewById(R.id.welcome_screen_right_button)).setEnabled(false);
		}
		else if (text.getText().toString().length() >= 4 && text.getText().toString().length() < 8) {
		  indicator.setImageResource(R.drawable.strength_half);
		  ((Button) getActivity().findViewById(R.id.welcome_screen_right_button)).setEnabled(true);
		}
		else if (text.getText().toString().length() >= 8) {
		  indicator.setImageResource(R.drawable.strength_full);
		  ((Button) getActivity().findViewById(R.id.welcome_screen_right_button)).setEnabled(true);
		}
	  }

	  public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	  }

	  public void onTextChanged(CharSequence s, int start, int before, int count) {

	  }
	});

	text.setOnFocusChangeListener(new OnFocusChangeListener() {

	  @Override
	  public void onFocusChange(View v, boolean hasFocus) {
		if (hasFocus) {
		  scroll.fullScroll(View.FOCUS_DOWN);
		}
	  }
	});
	return view;
  }

  @Override
  public void onResume() {
	super.onResume();
  }

  @Override
  public void onPause() {
	super.onPause();
  }

  @Override
  public void onDestroy() {
	super.onDestroy();
  }
}
