/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */
package de.r2soft.phoneguard.setup;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import de.r2soft.phoneguard.R;
import de.r2soft.phoneguard.core.LoginActivity;
import de.r2soft.phoneguard.core.MainActivity;
import de.r2soft.phoneguard.settings.AppSettings;
import de.r2soft.phoneguard.util.PasswordHash;
import de.r2soft.phoneguard.util.Utility;

/**
 * This container activity wraps multiple welcome fragments to create a menu that guides the user
 * trough the setup process of the application.
 * 
 * @author Katharina
 * 
 */
public class SetupContainer extends Activity {

  private enum FRAGMENT {
	GREETING, PIN;
  }

  private FRAGMENT state;
  private EditText text;

  @Override
  protected void onCreate(Bundle states) {
	super.onCreate(states);
	setContentView(R.layout.dialog_welcome_container);

	getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	showFragment(FRAGMENT.GREETING);
	// Setup click listener
	findViewById(R.id.welcome_screen_left_button).setOnClickListener(leftButtonClickListener);
	findViewById(R.id.welcome_screen_right_button).setOnClickListener(rightButtonClickListener);
  }

  @Override
  public void onResume() {
	super.onResume();
  }

  @Override
  public void onPause() {
	super.onPause();
  }

  @Override
  public void onDestroy() {
	super.onDestroy();
	exit(state);
  }

  private OnClickListener leftButtonClickListener = new OnClickListener() {

	@Override
	public void onClick(View v) {
	  if (state == FRAGMENT.GREETING)
		exit(state);
	  else if (state == FRAGMENT.PIN)
		showFragment(FRAGMENT.GREETING);
	}
  };

  private OnClickListener rightButtonClickListener = new OnClickListener() {

	@Override
	public void onClick(View v) {
	  if (state == FRAGMENT.GREETING) {
		showFragment(FRAGMENT.PIN);
	  }
	  else if (state == FRAGMENT.PIN) {
		text = (EditText) findViewById(R.id.dialog_setup_field_pin);
		if (text.getText().toString() != null && text.getText().toString().length() >= 4) {

		  String raw = text.getText().toString();
		  String coded;
		  try {
			coded = PasswordHash.createHash(raw);
			AppSettings.setGlobalPin(coded, getApplicationContext());
			AppSettings.setGlobalFirst(false, getApplicationContext());
		  }
		  catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			new Exception(
				"A bunch of monkeys were dispatched to find an algorythm but they all came back with bananas");
		  }
		  catch (InvalidKeySpecException e) {
			e.printStackTrace();
		  }

		  Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		  startActivity(intent);
		  getFragmentManager().popBackStack();
		  Toast.makeText(getApplicationContext(), R.string.toast_setup_pin_saved,
			  Toast.LENGTH_SHORT).show();

		}
		else {
		  Toast.makeText(getApplicationContext(), R.string.toast_setup_pin_failed,
			  Toast.LENGTH_LONG).show();
		}

	  }

	}
  };

  private void showFragment(FRAGMENT state) {
	FragmentTransaction trans = getFragmentManager().beginTransaction();
	Fragment fragment = null;

	String tag = getString(R.string.dialog_welcome_placeholder_tag);
	String title = getString(R.string.dialog_welcome_placeholder_title);
	String lButton = getString(R.string.dialog_welcome_placeholder_button_back);
	String rButton = getString(R.string.dialog_welcome_placeholder_button_next);

	if (state == FRAGMENT.GREETING) {
	  fragment = new Welcome_1_Greeting();
	  title = getString(R.string.dialog_welcome_1_title);
	  lButton = getString(R.string.dialog_welcome_1_button_back);
	  rButton = getString(R.string.dialog_welcome_1_button_next);
	  if (AppSettings.getGlobalPin(getApplicationContext()) == null)
		((Button) findViewById(R.id.welcome_screen_left_button)).setEnabled(false);
	  else
		((Button) findViewById(R.id.welcome_screen_left_button)).setEnabled(true);
	  ((Button) findViewById(R.id.welcome_screen_right_button)).setEnabled(true);
	}
	else if (state == FRAGMENT.PIN) {
	  fragment = new Welcome_2_PIN();
	  title = getString(R.string.dialog_welcome_2_title);
	  lButton = getString(R.string.dialog_welcome_2_button_back);
	  rButton = getString(R.string.dialog_welcome_2_button_next);
	  ((Button) findViewById(R.id.welcome_screen_left_button)).setEnabled(true);
	}

	/** Take info from above and use in fragment creation */
	trans.replace(R.id.welcome_screen_fragment_container, fragment, tag).commit();
	setTitle(title);
	((Button) findViewById(R.id.welcome_screen_left_button)).setText(lButton);
	((Button) findViewById(R.id.welcome_screen_right_button)).setText(rButton);
	this.state = state;
  }

  private void exit(FRAGMENT state) {
	finish();
  }

}
