/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */
package de.r2soft.phoneguard.util;

import android.location.Location;

/**
 * 
 * @author Katharina
 * 
 */
public class Utility {

  private static String HashKey;

  /**
   * Will take a user inputed PIN and convert it into a HashCode. This method is being called on PIN
   * input as well as in the database entry. The database will then check for the HashCode, not the
   * raw String
   * 
   * @param StringKey
   *          User PIN as a raw String
   * 
   * @return The user inputed PIN as a HashCode encrypted String. Is not reverse-engineerable. Used
   *         to write and read from the database
   */
  @Deprecated
  public static String makeHashKey(String StringKey) {

	Integer temp = StringKey.hashCode();
	HashKey = (String) temp.toString();

	return HashKey;
  }

  public static final String RAW = "RAW_";
  public static final String LAT = "LAT_";
  public static final String LOG = "LOG_";

  /** Takes a location object and puts it into a savable String format */
  public static String locationToString(Location location) {
	StringBuilder s = new StringBuilder();
	s.append(location.getProvider());
	s.append(RAW);
	s.append("-");

	s.append(location.getLatitude());
	s.append(LAT);
	s.append("-");

	s.append(location.getLongitude());
	s.append(LOG);
	return s.toString();
  }

  /** Decrypts string from database back into actual location object */
  public static Location stringToLocation(String data) {
	int indexRaw = data.indexOf(RAW);
	int indexLat = data.indexOf(LAT);
	int indexLog = data.indexOf(LOG);

	Location loc = new Location(data.substring(0, indexRaw));
	loc.setLatitude(Long.parseLong((String) data.subSequence(indexRaw + 1, indexLat)));
	loc.setLongitude(Long.parseLong((String) data.subSequence(indexLat + 1, indexLog)));

	return loc;
  }

}
