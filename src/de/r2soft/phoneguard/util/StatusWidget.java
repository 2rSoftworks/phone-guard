/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */
package de.r2soft.phoneguard.util;

import java.util.logging.Logger;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import de.r2soft.phoneguard.R;
import de.r2soft.phoneguard.core.GuardianService;
import de.r2soft.phoneguard.panic.PanicActivity;
import de.r2soft.phoneguard.settings.AppSettings;

public class StatusWidget extends AppWidgetProvider {

  private final Logger log = Logger.getLogger(getClass().getSimpleName());

  @Override
  public void onEnabled(Context context) {
	log.info("Creating the " + StatusWidget.class.getSimpleName() + " for the first time");
  }

  @Override
  public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
	log.info("Updating the " + StatusWidget.class.getSimpleName());

	// Register an onClickListener for the image button
	Intent signal = new Intent(context, StatusWidget.class);
	signal.setAction(Intents.ACTION_WIDGET_BUTTON);
	PendingIntent pendingSignal = PendingIntent.getBroadcast(context, 0, signal, 0);
	RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_view);
	remoteViews.setOnClickPendingIntent(R.id.widget_button_toggle, pendingSignal);

	remoteViews.setImageViewResource(R.id.widget_button_toggle, getWidgetIcon(context));

	// Update widget at the regular end of the cycle
	appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
  }

  /**
   * This onReceive handles the onClick of the ImageButton in the widget.
   */
  @Override
  public void onReceive(Context context, Intent intent) {
	// If someone pressed the widget button then process the event locally
	if (intent.getAction().equals(Intents.ACTION_WIDGET_BUTTON)) {

	  // Is the application currently active?
	  if (AppSettings.isCoreEnabled(context)) {
		// Send the user to the activity to stop the service
		Intent panic = new Intent(context, PanicActivity.class);
		panic.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(panic);
	  }
	  else {
		// Simply start the service from here and update the settings
		Intent service = new Intent(context, GuardianService.class);
		context.getApplicationContext().startService(service);
		AppSettings.setCoreEnabled(true, context);
	  }
	  forceWidgetUpdate(context);
	}

	// Pass other actions on to the super class
	super.onReceive(context, intent);
  }

  @Override
  public void onDisabled(Context context) {
	context.unregisterReceiver(this);
  }

  /** Private utility function **/

  /**
   * Load the application state and update the widget icon.
   * 
   * @param context
   */
  private int getWidgetIcon(Context context) {
	// Load the remote view
	RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_view);

	// Load correct application icon
	boolean isAppEnabled = AppSettings.isCoreEnabled(context);
	log.info("App is " + isAppEnabled);

	if (isAppEnabled)
	  return R.drawable.icon_guarding_phone;
	else
	  return R.drawable.icon_disabled;
  }

  /**
   * Forces the widget to re-draw itself outside of the regular update cycle.
   * 
   * @param context
   */
  private void forceWidgetUpdate(Context context) {
	AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
	ComponentName thisAppWidget = new ComponentName(context.getPackageName(),
		StatusWidget.class.getName());
	int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);

	onUpdate(context, appWidgetManager, appWidgetIds);
  }

}
