/* #########################################################################
 * Copyright (c) 2013 Random Robot Softworks
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ######################################################################### */
package de.r2soft.phoneguard.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import de.r2soft.phoneguard.R;
import de.r2soft.phoneguard.util.Utility;

public class AppSettings {

  private static Context context;

  /*
   * #################### Core functionality ####################
   */

  /** @@@@@@@@@@@@@@ Global App status */
  public static void setCoreEnabled(boolean state, Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	prefs.edit().putBoolean(context.getString(R.string.key_core_enabled), state).commit();
  }

  public static boolean isCoreEnabled(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	return prefs.getBoolean(context.getString(R.string.key_core_enabled), false);
  }

  /** @@@@@@@@@@@@@@ Silent Alarm Flag */
  public static void setCoreSilent(boolean state, Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	prefs.edit().putBoolean(context.getString(R.string.key_core_silent), state);
  }

  public static boolean isCoreSilent(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	return prefs.getBoolean(context.getString(R.string.key_core_silent), false);

  }

  /*
   * #################### Global Preferences ####################
   */

  /** @@@@@@@@@@@@@@ First launch flag */
  public static void setGlobalFirst(boolean state, Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	prefs.edit().putBoolean(context.getString(R.string.key_global_first), state).commit();
  }

  public static boolean isGlobalFirst(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	return prefs.getBoolean(context.getString(R.string.key_global_first), true);
  }

  /** @@@@@@@@@@@@@@ Application PIN */
  public static void setGlobalPin(String code, Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	prefs.edit().putString(context.getString(R.string.key_global_pin), code).commit();
  }

  public static String getGlobalPin(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	return prefs.getString(context.getString(R.string.key_global_pin), null);
  }

  /*
   * #################### Global Preferences ####################
   */

  /** @@@@@@@@@@@@@@ Location tracking flag */
  public static void setLocationEnabled(boolean state, Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	prefs.edit().putBoolean(context.getString(R.string.key_location_enabled), state).commit();
  }

  public static boolean isLocationEnabled(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	return prefs.getBoolean(context.getString(R.string.key_location_enabled), false);
  }

  /** @@@@@@@@@@@@@@ Directional guidance flag */
  public static void setLocationDirectional(boolean state, Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	prefs.edit().putBoolean(context.getString(R.string.key_location_directional), state).commit();
  }

  public static boolean isLocationDirectional(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	return prefs.getBoolean(context.getString(R.string.key_location_directional), false);
  }

  /** @@@@@@@@@@@@@@ Last Position to be sent to used */
  public static void setLocationDirectionalLast(Location locations, Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);

	String data = Utility.locationToString(locations);

	prefs.edit().putString(context.getString(R.string.key_location_directional_last), data)
		.commit();
  }

  public static Location getLocationDirectionalLast(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);

	String data = prefs.getString(context.getString(R.string.key_location_directional_last), null);

	return Utility.stringToLocation(data);
  }

  public static void deleteLastLocation() {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	prefs.edit().putString(context.getString(R.string.key_location_directional_last), null)
		.commit();
  }

  /** @@@@@@@@@@@@@@ Phone number */
  public static void setLocationPhonenumber(String number, Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	prefs.edit().putString(context.getString(R.string.key_location_phonenumber), number).commit();
  }

  public static String getLocationPhonenumber(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	return prefs.getString(context.getString(R.string.key_location_phonenumber), null);
  }

  /*
   * #################### Donation Preferences ####################
   */

  /** @@@@@@@@@@@@@@ Should the donation dialog be displayed to the user */
  public static boolean isDonateAsk(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	return prefs.getBoolean(context.getString(R.string.key_donate_ask), false);
  }

  public static void setDonateAsk(boolean state, Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	prefs.edit().putBoolean(context.getString(R.string.key_donate_ask), state).commit();
  }

  /** @@@@@@@@@@@@@@ Did the user donate something? :) */
  public static boolean isDonateDone(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	return prefs.getBoolean(context.getString(R.string.key_donate_done), false);
  }

  public static void setDonateDone(boolean state, Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	prefs.edit().putBoolean(context.getString(R.string.key_donate_done), state).commit();
  }

  /** @@@@@@@@@@@@@@ Counts the amount of times a user has opened and closed the application */
  public static int getUsageCounter(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	return prefs.getInt(context.getString(R.string.key_donate_counter), 0);
  }

  /** Increments the usage counter */
  public static void incrementUsageCounter(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	int temp = prefs.getInt(context.getString(R.string.key_donate_counter), 0);
	prefs.edit().putInt(context.getString(R.string.key_donate_counter), ++temp).commit();
  }

  /** ONLY CALL THIS ON MAJOR APPLICATION UPDATES! */
  @Deprecated
  public static void resetUsageCount(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	prefs.edit().putInt(context.getString(R.string.key_donate_counter), 0).commit();
  }

  /** @@@@@@@@@@@@@@ Determines if the usage count should be used or not */
  public static boolean isCounterState(Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	return prefs.getBoolean(context.getString(R.string.key_donate_counter_state), true);
  }

  public static void setCounterState(boolean state, Context context) {
	SharedPreferences prefs = context.getSharedPreferences(
		context.getString(R.string.key_global_name), Context.MODE_PRIVATE);
	prefs.edit().putBoolean(context.getString(R.string.key_donate_counter_state), state).commit();
  }

}
